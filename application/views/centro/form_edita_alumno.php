<?php ?>
<div class="bg-warnig">     
    <?php echo validation_errors(); ?>
</div>
<?php echo form_open(site_url('centro/show_form_alumno/'.$alumno->NIA));?>
    <div class="form-row">
        <div class="form-group col-md-4">
            <label for="nombre">Nombre: </label>
            <?php echo form_input('nombre',$alumno->nom,['class'=>'form-control','id'=>'nombre', 'place_holder'=>'nombre']); ?>
        </div>
         <div class="form-group col-md-4">
            <label for="apellido1">1er Apellido: </label>
            <?php echo form_input('apellido1',$alumno->apellido1,['class'=>'form-control','id'=>'apellido1', 'place_holder'=>'Primer apellido']); ?>
        </div>
        <div class="form-group col-md-4">
            <label for="apellido2">2º Apellido: </label>
            <?php echo form_input('apellido2',$alumno->apellido2,['class'=>'form-control','id'=>'apellido2', 'place_holder'=>'Segundo Apellido']); ?>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="email">correo electrónico: </label>
            <?php echo form_input('email',$alumno->email,['class'=>'form-control','id'=>'email', 'place_holder'=>'Correo electrónico']); ?>
        </div>
        <div class="form-group col-md-2">
            <label for="fecha_nac">Fecha nacimiento: </label>
            <!-- <?php echo form_input('fecha_nac',$alumno->fecha_nac,['class'=>'form-control','id'=>'fecha_nac', 'place_holder'=>'Fecha de nacimiento']); ?>-->
            <input type="date" name="fecha_nac" id="fecha_nac" value="<?php echo $alumno->fecha_nac; ?>" class="form-control">
        </div>
        <div class="form-group col-md-2">
            <label for="nif">NIF: </label>
            <?php echo form_input('nif',$alumno->nif,['class'=>'form-control','id'=>'nif', 'place_holder'=>'NIF o NIE']); ?>
        </div>
        <div class="form-group col-md-2">
            <label for="NIA">NIA: </label>
            <?php echo form_input('NIA',$alumno->NIA,['class'=>'form-control','id'=>'NIA', 'place_holder'=>'NIA', 'readonly'=> True]); ?>
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Enviar</button>
    <a href="<?php echo site_url('centro/todos_alumnos');?>" class="btn btn-secondary">Volver</a>
<?php echo form_close(); ?>

