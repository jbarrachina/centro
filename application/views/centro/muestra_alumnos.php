
<table id="table" class="table table-striped">
    <thead>
        <tr>
            <th>nombre</th>
            <th>email</th>
            <th>NIF</th>
            <th>NIA</th>
            <th>grupo</th>
            <th>curso</th>
            <th>acciones</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($alumnos as $alumno) { ?>
        <tr>
            <td>
                <?php echo $alumno->nombre_completo; ?>
            </td>
            <td>
                <?php echo $alumno->email; ?>
            </td>
            <td>
                <?php echo $alumno->nif; ?>
            </td>
            <td>
                <?php echo $alumno->NIA; ?>
            </td>
            <td>
                <?php echo $alumno->codigo; ?>
            </td>
            <td>
                <?php echo $alumno->curso; ?>
            </td>
            <td>
                <a href="<?php echo site_url('centro/show_form_alumno/'.$alumno->NIA);?>" title="edita alumno">                  
                    <span class="fas fa-edit" aria-hidden="true"></span>
                </a>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>


