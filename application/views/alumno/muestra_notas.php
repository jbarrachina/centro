
<table id="table" class="table table-striped">
    <thead>
        <tr>
            <th>Módulos</th>
            <th>Notas</th>
        </tr>
    </thead>
    <tbody>
        <?php $suma = 0; $num_modulos=0;?>
        <?php foreach ($notas as $modulo) { ?>
        <tr>
            <td>               
                <?php echo $modulo->nombre_cas; ?>             
            </td>
            <td>
                <?php echo $modulo->nota; ?>
                <?php $suma += $modulo->nota; $num_modulos++; ?>
            </td>
        </tr>
        <?php } ?>
        
    </tbody>
</table>

<div class="row justify-content-end alert alert-primary">
    <div class=" col-md-3 text-primary"><h3> Nota media </h3> </div>
    <div class="col-md-1 text-secondary"><h3><?php echo ($num_modulos > 0) ? $suma/$num_modulos : ' - ';?></h3></div>
</div>
<a href="<?php echo site_url();?>" class="btn btn-primary">Volver</a>
