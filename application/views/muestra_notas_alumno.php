
<?php echo validation_errors('<div class="bg-warning">','</div>'); ?>
<?php echo form_open(site_url('centro/show_form_notas_alumno/'.$grupo.'/'.$alumno->NIA)); ?>
<table id="table" class="table table-striped">
    <thead>
        <tr>
            <th>Módulo</th>
            <th>Notas</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($modulos as $modulo) { ?>
        <tr>
            <td>
                <?php echo $modulo->nombre_cas; ?>
            </td>
            <td>
                <?php echo form_input('notas['.$modulo->id.']', set_value('notas['.$modulo->id.']', isset($notas[$modulo->id]) ? $notas[$modulo->id] : ''));?>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<button type="submit" class="btn btn-primary">Enviar</button>
<a href="<?php echo site_url('centro/alumnos/'.$grupo); ?>" class="btn btn-secondary">Volver</a>
<?php echo form_close();?>


