
<?php echo validation_errors('<div class="bg-warning">','</div>'); ?>
<?php echo form_open(site_url('centro/show_notas_alumnos/'.$grupo->id.'/'.$contenido)); ?>
<table id="table" class="table table-striped">
    <thead>
        <tr>
            <th>nombre</th>
            <th>email</th>
            <th>NIA</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($alumnos as $alumno) { ?>
        <tr>
            <td>
                <?php echo $alumno->nombre; ?>
            </td>
            <td>
                <?php echo form_input('notas['.$alumno->NIA.']', set_value('notas['.$alumno->NIA.']', ''));?>
            </td>
            <td>
                <?php echo $alumno->NIA; ?>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<button type="submit" class="btn btn-primary">Enviar</button>
<?php echo form_close();?>


