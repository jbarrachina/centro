
<table id="table" class="table table-striped">
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($modulos as $modulo) { ?>
        <tr>
            <td>               
                <?php echo $modulo->nombre_cas; ?>             
            </td>
            <td>
                <a href="<?php echo site_url('centro/show_notas_alumnos/'.$grupo->id.'/'.$modulo->id);?>">                  
                    <span class="fas fa-list-ul " aria-hidden="true"></span>
                </a>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<a href="<?php echo site_url();?>" class="btn btn-primary">Volver</a>
