
<?php echo validation_errors('<div class="bg-warning">','</div>'); ?>

<!--<form action="" method="post">-->
<?php echo form_open(site_url("centro/show_form_acta_eleccion_delegados/$grupo"));?>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="delegado">Delegado: </label>
            <!--<input type="text" class="form-control" id="delegado" name="delegado" placeholder="Introduce el NIA">-->
            <?php echo form_dropdown('delegado', $alumnos, '', ['id'=>'delegado', 'class'=>'form-control', 'placeholder'=> 'Introduce el NIA']); ?>
        </div>
        <div class="form-group col-md-6">
            <label for="votos_delegado">Votos: </label>
            <!--<input type="numeric" class="form-control" id="votos_delegado" name="delegado" placeholder="votos obtenidos por el delegado">-->
            <?php echo form_input('votos_delegado',0,['class'=>'form-control','id'=>'votos_delegado', 'place_holder'=>'votos obtenido por el delegado']); ?>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="subdelegado">Subdelegado: </label>
            <!--<input type="text" class="form-control" id="subdelegado" name="subdelegado" placeholder="Introduce el NIA">-->
            <?php echo form_dropdown('subdelegado', $alumnos, '', ['id'=>'subdelegado', 'class'=>'form-control', 'placeholder'=> 'Introduce el NIA']); ?>
        </div>
        <div class="form-group col-md-6">
            <label for="votos_subdelegado">Votos: </label>
            <!--<input type="numeric" class="form-control" id="voto_subdelegado" name="subdelegado" placeholder="votos obtenidos por el subdelegado">-->
            <?php echo form_input('votos_subdelegado',0,['class'=>'form-control','id'=>'votos_subdelegado', 'place_holder'=>'votos obtenido por el subdelegado']); ?>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-3">
            <label for="alumnos_presentes">Alumnos presentes: </label>
            <input type="numeric" class="form-control" id="alumnos_presentes" name="alumnos_presentes" placeholder="Alumnos presentes">
        </div>
        <div class="form-group col-md-3">
            <label for="votos_emitidos">Votos emitidos: </label>
            <input type="numeric" class="form-control" id="votos_emitidos" name="votos_emitidos" placeholder="Votos emitidos">
        </div>
        <div class="form-group col-md-3">
            <label for="votos_validos">Votos válidos: </label>
            <input type="numeric" class="form-control" id="votos_validos" name="votos_validos" placeholder="Votos válidos">
        </div>
        <div class="form-group col-md-3">
            <label for="fecha_eleccion">Fecha elección: </label>
            <input type="date" class="form-control" id="fecha_eleccion" name="fecha_eleccion" placeholder="Fecha eleccion">
        </div>
    </div>    
    <button type="submit" class="btn btn-primary">Enviar</button>
    <a href="<?php echo site_url('centro');?>" class="btn btn-secondary">Volver</a>
<?php echo form_close(); ?>

