<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.18/datatables.min.css"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous"> 
        <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.18/datatables.min.js"></script>
        <title>
            <?php echo $titulo; ?>
        </title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <h2 class="text-primary"><?php echo $titulo; ?></h2>
                </div>
                <div class="col-md-3">
                    <span class="fas fa-user"></span>
                    <?php  $user = $this->ion_auth->user()->row(); echo $user->first_name; ?> 
                    <a href="<?php echo site_url('auth/logout'); ?>">
                    <span class="fas fa-sign-out-alt"></span>
                    </a>
                </div>
            </div>
           
           
