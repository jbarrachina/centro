
<table id="table" class="table table-striped">
    <thead>
        <tr>
            <th>nombre</th>
            <th>email</th>
            <th>NIF</th>
            <th>NIA</th>
            <th>acciones</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($alumnos as $alumno) { ?>
        <tr>
            <td>
                <?php echo $alumno->nombre; ?>
            </td>
            <td>
                <?php echo $alumno->email; ?>
            </td>
            <td>
                <?php echo $alumno->nif; ?>
            </td>
            <td>
                <?php echo $alumno->NIA; ?>
            </td>
            <td>
                <a href="<?php echo site_url('centro/show_form_notas_alumno/'.$grupo.'/'.$alumno->NIA);?>" title="notas de módulos">                  
                    <span class="fas fa-edit" aria-hidden="true"></span>
                </a>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<a href="<?php echo site_url('centro/registra_grupo/'.$grupo); ?>" class="btn btn-secondary">Registra alumnos</a>


