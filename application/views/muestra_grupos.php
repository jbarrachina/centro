<div class="text-center">
    <a href="<?php echo site_url('centro/todos_alumnos/'); ?>" title="listado de alumnos" class="btn btn-primary">listado de alumnos</a>
</div>
<table id="table" class="table table-striped">
    <thead>
        <tr>
            <th>Código</th>
            <th>Nombre</th>
            <th>Familia</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($grupos as $grupo) : ?>
        <tr>
            <td>             
                <?php echo $grupo->codigo; ?>
            </td>
            <td>               
                <?php echo $grupo->nombre_largo; ?>             
            </td>
            <td>
                <?php echo $grupo->familia; ?>
            </td>
            <td>
                <a href="<?php echo site_url('centro/alumnos/'.$grupo->id); ?>" title="listado de alumnos">               
                    <span class="fas fa-users" aria-hidden="true"></span>
                </a>
                <?php if ($this->ion_auth->is_admin()): ?>
                <a href="<?php echo site_url('centro/show_form_acta_eleccion_delegados/'.$grupo->id);?>" title="acta elección delegados">                  
                    <span class="fas fa-poll <?php echo $grupo->delegado==''?'text-danger': '';?>" aria-hidden="true"></span>
                </a>
                <?php endif; ?>
                <a href="<?php echo site_url('centro/show_modulos/'.$grupo->id);?>" title="listado de módulos">                  
                    <span class="fas fa-graduation-cap" aria-hidden="true"></span>
                </a>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
