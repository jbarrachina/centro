<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Alumno
 *
 * @author jose
 */
class Alumno extends CI_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->model('modelo_centro');
        $this->load->library('ion_auth');
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login');
        }
    }  
    
    public function index() {
        $user = $this->ion_auth->user()->row();
        //recupero el NIA del alumno
        $alumno = $this->modelo_centro->get_alumno_nif($user->username);
        //recupero el curso (o cursos) en el cual está matriculado
        $alumno = $this->modelo_centro->get_alumno($alumno->NIA);
        $data['titulo'] = "Notas de $alumno->nombre";
        $data['alumno'] = $alumno;
        $data['notas'] = $this->modelo_centro->get_boletinnotas_alumno($alumno->grupo, $alumno->NIA);
        $this->load->view('commons/header',$data);
        $this->load->view('alumno/muestra_notas',$data);
        $this->load->view('commons/footer');
        
    }
}
