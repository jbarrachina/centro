<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Centro
 *
 * @author jbarrachinab
 */
class Centro extends CI_Controller{
    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->model('modelo_centro');
        $this->load->library('ion_auth');
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login');
        } elseif ($this->ion_auth->in_group(4)) {
            redirect(site_url('alumno'));
        }
    }  

    public function index() {
        $data['titulo'] = 'Listado de Grupos';
        $data['grupos'] = $this->modelo_centro->get_grupos();
        $this->load->view('commons/header',$data);
        $this->load->view('muestra_grupos',$data);   
        $this->load->view('commons/footer');
        /*        
        echo "<pre>";
        print_r($data['grupos'][3]->nombre);
        echo "</pre>";
         */
    }
    
    public function logout(){
        $this->ion_auth->logout();
        redirect();
    }
    
    public function alumnos($id_grupo){
        $data['titulo'] = 'Listado de Alumnos';
        $data['alumnos'] = $this->modelo_centro->get_alumnos($id_grupo);
        $data['grupo'] = $id_grupo;
        $this->load->view('commons/header',$data);
        $this->load->view('muestra_alumnos',$data);   
        $this->load->view('commons/footer');
        /*echo "<pre>";
        print_r($data['alumnos']);
        echo "</pre>";*/        
    } 
    
    public function todos_alumnos(){
        $data['titulo'] = 'Listado de Alumnos del Centro';
        $data['alumnos'] = $this->modelo_centro->get_todos_alumnos();
        $this->load->view('commons/header',$data);
        $this->load->view('centro/muestra_alumnos',$data);   
        $this->load->view('commons/footer');        
    }
    
    public function show_form_alumno($nia){
        $this->load->helper('form');
        $this->load->library('form_validation');
        $data['titulo'] = 'Modifica de Alumno'; 
        $this->form_validation->set_rules('nombre','aombre del alumno','required|mb_strtoupper|trim',['required'=>'No puedes dejar el %s en blanco']);
        $this->form_validation->set_rules('apellido1','primer apellido del alumno','required|mb_strtoupper|trim',['required'=>'No puedes dejar el %s en blanco']);
        $this->form_validation->set_rules('apellido2','Nombre del alumno','mb_strtoupper|trim');
        
        $this->form_validation->set_rules('email','correo electrónico','required|valid_email|trim');
        $this->form_validation->set_rules('nif','documento de identificación','required|callback_dni_ok|mb_strtoupper|trim|min_length[9]|alpha_numeric');
        $this->form_validation->set_rules('NIA','NIA','required|trim|exact_length[8]|numeric'); //sólo haría falta en el alta.
        
        if ($this->form_validation->run()===FALSE){
           $data['alumno'] = $this->modelo_centro->get_alumno($nia);
           $this->load->view('commons/header',$data);
           $this->load->view('centro/form_edita_alumno',$data);   
           $this->load->view('commons/footer');
        } else { //proceso los datos introducidos en el formulario
            $alumno = ['nif'=>$this->input->post('nif'),
                'fecha_nac' => $this->input->post('fecha_nac'),
                'email' => $this->input->post('email'),
                'nombre' => $this->input->post('nombre'),
                'apellido1' => $this->input->post('apellido1'),
                'apellido2' =>$this->input->post('apellido2')];
            echo '<pre>';
            print_r($alumno); //ahora lo muestro
            echo '</pre>';
            //$this->modelo_centro->actuliza_alumno($nia, $alumno);
        }
        
        
    }
            
    
    public function show_form_acta_eleccion_delegados($id_grupo){
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('votos_delegado','Votos del delegado','required|greater_than[5]');
        
        $data['titulo'] = 'Formulario Elección de Delegados';
        $data['grupo'] = $id_grupo;
        
        if ($this->form_validation->run() == FALSE){
            $this->load->view('commons/header',$data);
            $data['alumnos'] = $this->modelo_centro->get_alumnos($id_grupo, TRUE);
            $this->load->view('form_acta_eleccion_delegados',$data);
            $this->load->view('commons/footer');
        } else { //todo va bien
            $acta = [
                'grupo'=>$id_grupo,
                'delegado'=>$this->input->post('delegado'),
                'votos_delegado'=>$this->input->post('votos_delegado'),
                'subdelegado'=>$this->input->post('subdelegado'),
                'votos_subdelegado'=>$this->input->post('votos_subdelegado'),
                'alumnos_presentes'=>$this->input->post('alumnos_presentes'),
                'votos_emitidos'=>$this->input->post('votos_emitidos'),
                'votos_validos'=>$this->input->post('votos_validos'),
                'fecha_votacion'=>$this->input->post('fecha_eleccion') 
            ];
            $this->modelo_centro->add_acta($acta);
            redirect(site_url());
        }      
    }
    
    public function show_form_notas_alumno($grupo, $nia) {
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $data['alumno'] = $this->modelo_centro->get_alumno($nia);
        $data['titulo'] = "Notas de ".$data['alumno']->nombre;
        $data['modulos'] = $this->modelo_centro->get_modulos($grupo);
        $data['notas'] = $this->modelo_centro->get_notas_alumno($grupo, $nia);
        $data['grupo'] = $grupo;
        $this->form_validation->set_rules('notas[]',' notas ', 'required|is_natural|greater_than_equal_to[0]|less_than_equal_to[10]');
        if ($this->form_validation->run()===FALSE){
            $this->load->view('commons/header',$data);
            $this->load->view('muestra_notas_alumno',$data);
            //print_r($data['notas']);
            $this->load->view('commons/footer');
        } else {
           $notas = $this->input->post('notas');
           foreach ($notas as $id => $nota) {
              $registro = ['NIA'=>$nia,'grupo'=>$grupo,'contenido'=>$id,'nota'=>$nota]; 
              $this->modelo_centro->add_nota($registro);
           }
           redirect(site_url('centro/alumnos/'.$grupo));
        }
    }
    
    public function show_modulos($id_grupo){
        $grupo = $this->modelo_centro->get_grupo($id_grupo);
        $data['titulo'] = ' Módulos de '.$grupo->nombre_largo; 
        $this->load->view('commons/header',$data);
        $data['grupo'] = $grupo;
        $data['modulos'] = $this->modelo_centro->get_modulos($id_grupo);
        $this->load->view('muestra_modulos',$data);
        $this->load->view('commons/footer');
    }
    
    public function show_notas_alumnos($id_grupo, $id_contenido){
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $data['alumnos'] = $this->modelo_centro->get_alumnos($id_grupo);
        $data['titulo'] = 'Listado de Alumnos';
        $this->form_validation->set_rules('notas[]',' notas ', 'required|is_natural|greater_than_equal_to[0]|less_than_equal_to[10]');
        if ($this->form_validation->run() == FALSE){          
            $data['grupo'] = $this->modelo_centro->get_grupo($id_grupo);
            $data['contenido'] = $id_contenido; 
            $this->load->view('commons/header',$data);
            $this->load->view('muestra_notas_alumnos',$data);   
            $this->load->view('commons/footer');
        } else {
           $notas = $this->input->post('notas');
           foreach ($notas as $nia => $nota) {
              $registro = ['NIA'=>$nia,'grupo'=>$id_grupo,'contenido'=>$id_contenido,'nota'=>$nota]; 
              $this->modelo_centro->add_nota($registro);
           }
           redirect(site_url('centro/show_modulos/'.$id_grupo));
        }
    }
    
    public function registra_grupo($grupo) {
        $alumnos = $this->modelo_centro->get_alumnos($grupo);
        foreach($alumnos as $alumno) {
            $username = $alumno->nif;
            $password = $alumno->NIA;
            $email = $alumno->email;
            $adicional = ['first_name'=>$alumno->nombre];
            $group = [4]; //el identificador del grupo alumnosú
            $this->ion_auth->register($username, $password, $email, $adicional, $group);
            //print_r($alumno);
        }
        redirect(site_url('centro/alumnos/'.$grupo));
    }
    
    public function send_email(){
        $this->load->library('email');

        $this->email->from('admin_iaw', 'Administrador');
        $this->email->to('jbarrachina@ausiasmarch.net');

        $this->email->subject('Prueba');
        $this->email->message('Veamos si funciona.');

        if ( ! $this->email->send())
        {
              $this->email->print_debugger(array('headers'));
        } else {
            redirect('/','refresh');
        }
    }
    
/*
 * Funciones de comprobación
 */
    
    function dni_ok($dni) {
        $dni = strtoupper($dni);
        //veamos la estructura del dato introducido
        $patron = '/^[X,Y,Z,0-9][0-9]{7}[A-Z]$/';
        if (preg_match($patron, $dni) == 0) { //no coincide el patrón.
            $this->form_validation->set_message('dni_ok', "Nº de documento, $dni, mal formado. {field} erróneo ");
            return FALSE;
        }
        //veamos si es un NIE
        switch ($dni[0]) {
            case 'X': $dni = '0' . substr($dni, 1);
                break;
            case 'Y': $dni = '1' . substr($dni, 1);
                break;
            case 'Z': $dni = '2' . substr($dni, 1);
                break;
        }

        $formula = "TRWAGMYFPDXBNJZSQVHLCKE";
        $letra = substr($dni, -1);
        $numero = substr($dni, 0, strlen($dni) - 1);
        if ($letra !== $formula[$numero % 23]) {
            $this->form_validation->set_message('dni_ok', "Letra: $letra del documento $numero {field} erróneo ");
            return FALSE;
        } else {
            return TRUE;
        }
    }

}
