<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Modelo_centro
 *
 * @author jbarrachinab
 */
class Modelo_centro extends CI_Model {
    //put your code here
    public function get_grupos() {
        $sql = <<< SQL
          SELECT id, codigo, nombre_largo, familia, delegado
              FROM grupo_plus 
              LEFT JOIN actas ON grupo_plus.id = actas.grupo    
SQL;
        $consulta = $this->db->query($sql);
        return $consulta->result();
    }
    
    public function get_grupo($id_grupo) {
        $sql = <<< SQL
          SELECT id, codigo, nombre_largo, familia
              FROM grupo_plus 
              WHERE id = ?  
SQL;
        $consulta = $this->db->query($sql, [$id_grupo]);
        $listadeungrupo = $consulta->result();
        return $listadeungrupo[0]; //en lugar de un vector de un objeto pasa el objeto
    }
    
    /*
     */
    public function get_todos_alumnos() {
        $sql = <<< SQL
          SELECT alumnos.NIA, CONCAT(apellido1, ' ',apellido2, ', ',alumnos.nombre) as nombre_completo, 
              nombre, apellido1, apellido2, email, fecha_nac, nif, 
              codigo, nombre_largo as curso, familia             
            FROM matricula 
            LEFT JOIN alumnos ON alumnos.NIA = matricula.NIA
            LEFT JOIN grupo_plus ON grupo_plus.id = matricula.id_grupo                
SQL;
        $consulta = $this->db->query($sql);
        return $consulta->result(); 
    }
    
    public function get_alumnos($grupo, $dropdown=FALSE) {
        $sql = <<< SQL
          SELECT alumnos.NIA, CONCAT(apellido1, ' ',apellido2, ', ',alumnos.nombre) as nombre, email, fecha_nac, nif, grupos.codigo as grupo 
            FROM matricula 
            LEFT JOIN alumnos ON alumnos.NIA = matricula.NIA
            LEFT JOIN grupos ON grupos.id = matricula.id_grupo
            WHERE  grupos.id = ?                
SQL;
        $consulta = $this->db->query($sql,[$grupo]);
        if ($dropdown==FALSE) {
          return $consulta->result();
        } else {
            $alumnos = [];
            foreach ($consulta->result() as $alumno){
                $alumnos[$alumno->NIA]=$alumno->nombre;
            }
            return $alumnos;
        }  
    }
    
    public function get_alumno($NIA){
        $sql = <<< SQL
            SELECT alumnos.NIA, CONCAT(apellido1, ' ',apellido2, ', ',alumnos.nombre) as nombre, 
                nombre as nom, apellido1, apellido2, email, fecha_nac, nif, id_grupo as grupo
                FROM alumnos
                LEFT JOIN matricula ON alumnos.NIA = matricula.NIA
                WHERE alumnos.NIA = ?
SQL;
        $consulta = $this->db->query($sql,[$NIA]);
        $alumnos = $consulta->result();
        return $alumnos[0]; //sólo coja el elemento del array.
    }
    
    public function get_alumno_nif($nif){
        $sql = <<< SQL
            SELECT NIA, CONCAT(apellido1, ' ',apellido2, ', ',alumnos.nombre) as nombre, email, fecha_nac, nif
                FROM alumnos
                WHERE nif = ?
SQL;
        $consulta = $this->db->query($sql,[$nif]);
        $alumnos = $consulta->result();
        return $alumnos[0]; //sólo coja el elemento del array.
    }
    
    public function actuliza_alumno($nia, $alumno) {
        $this->db->where(['NIA'=>$nia]); //que registro
        $this->db->upadte('alumnos', $alumno); //que tabla y con que valores
    }
    
    public function get_modulos($id_grupo){
        $sql = <<< SQL
            SELECT contenidos.id, contenidos.nombre_cas
                FROM contenidos
                LEFT JOIN cursos ON cursos.codigo = contenidos.curso
                LEFT JOIN cursos_grupos ON cursos.codigo = cursos_grupos.curso
                WHERE cursos_grupos.id_grupo = ? AND NOT contenidos.codigo IN ('TU01CF','TU02CF')
                AND contenidos.nombre_cas != 'Formación en Centros de Trabajo'
SQL;
        $consulta = $this->db->query($sql,[$id_grupo]);
        return $consulta->result();
    }
    
    public function get_notas_alumno($grupo, $nia){
        $sql = <<< SQL
            SELECT contenido, nota
                FROM notas
                WHERE NIA = ? AND grupo = ? 
SQL;
        $consulta = $this->db->query($sql,[$nia, $grupo]);
        $notas=[];
        foreach ($consulta->result() as $nota) {
            $notas[$nota->contenido] = $nota->nota;
        }
        return $notas;
    }
    
    public function get_boletinnotas_alumno($grupo, $nia){
        $sql = <<< SQL
            SELECT contenido, nota, nombre_cas
                FROM notas
                LEFT JOIN contenidos ON contenidos.id = notas.contenido
                WHERE NIA = ? AND grupo = ? 
SQL;
        $consulta = $this->db->query($sql,[$nia, $grupo]);
        return $consulta->result();
    }
    
    public function add_acta($acta){
        $this->db->replace('actas',$acta);
        
    }
    
    public function add_nota($registro){
        $this->db->replace('notas',$registro);      
    }
}
